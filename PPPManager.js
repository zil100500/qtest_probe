require('dotenv').config();
const EventEmitter = require('events').EventEmitter;
const { spawn, exec } = require('child_process');

class PPPManager extends EventEmitter{

    constructor() {

        super();

        this.tail = null;
        this.reconnectAttempts = 0;
        this.keepAlive = true;

        this.pppTerminatedPattern = /.*pppd\[\d+]: Exit\..*/m;
        this.pppConnectedPattern = /.*pppd\[\d+]: remote IP address \d+\.\d+\.\d+\.\d+.*/g;
        this.pppUnableDiscovery = /.* pppd\[\d+]: Unable to complete PPPoE Discovery.*/g;

    }

    /**
     * Hack
     * @returns {Promise<void>}
     */
    async initFatalDisconnect() {

        await this.stop();
        this.emit('fatal-disconnect');

    }

    /**
     * @returns {Promise<void>}
     * @emits PPPManager#stopped
     */
    async stop() {

        this.keepAlive = false;
        await this.switchOffPPPoE();
        /** @event PPPManager#stopped */
        this.emit('stopped');

    }

    /**
     * @fires PPPManager#fatal-disconnect
     * @fires PPPManager#started
     * @returns {Promise<void>}
     */
    async start() {

        try {
            await this.connect();
        }
        catch (error) {
            /** @event PPPManager#fatal-disconnect */
            this.emit('fatal-disconnect');
        }

        const disconnectHandler = async () => {
            try {
                this.removeListener('pppoe-disconnect', disconnectHandler);
                this.stopWatch();

                if (this.keepAlive) {
                    await this.connect();
                    this.on('pppoe-disconnect', disconnectHandler);
                    this.watch();
                }
            }
            catch (error) {
                /** @event PPPManager#fatal-disconnect */
                this.emit('fatal-disconnect');
            }
        };

        this.watch();
        this.on('pppoe-disconnect', disconnectHandler);

        /** @event PPPManager#started */
        this.emit('started');

    }

    /**
     * @returns {Promise<void>}
     */
    async connect() {

        while (++this.reconnectAttempts <= process.env.RETRY_CONNECT_ATTEMPTS) {
            await this.switchOffPPPoE();

            try {
                await this.switchOnPPPoE();
                this.reconnectAttempts = 0;
                return;
            }
            catch (error) {
                await this.wait(parseInt(process.env.RECONNECT_INTERVAL));
            }
        }

        throw new Error('Unable to connect pppoe.');

    }

    wait(t) {

        return new Promise(resolve => {
            setTimeout(() => {
                resolve();
            }, t);
        });

    }

    /**
     * @emits PPPManager#pppoe-disconnect
     */
    watch() {

        this.tail = spawn('tail', ['-f', '-n1', process.env.SYSLOG_PATH]);

        this.tail.stdout.on('data', data => {
            if (this.pppTerminatedPattern.test(data.toString())) {
                /** @event PPPManager#pppoe-disconnect */
                this.emit('pppoe-disconnect');
            }
        });

    }

    stopWatch() {

        try {
            this.tail.kill();
        }
        catch (error) {
            //DO nothing
        }

    }

    /**
     * @returns {Promise<unknown>}
     * @emits PPPManager#pppoe-connected
     */
    switchOnPPPoE() {

        return new Promise((resolve, reject) => {

            const pon = spawn('pon', [process.env.PPP_CONNECTION_NAME]);
            const tail = spawn('tail', ['-f', '-n1', process.env.SYSLOG_PATH]);

            pon.on('close', code => {
                if (code !== 0) {
                    pon.kill();
                    tail.kill();
                    reject('Another reason.');
                }
            });

            tail.stdout.on('data', data => {
                if (this.pppConnectedPattern.test(data.toString())) {
                    pon.kill();
                    tail.kill();
                    /** @event PPPManager#pppoe-connected */
                    this.emit('pppoe-connected');
                    resolve();
                }

                if (this.pppUnableDiscovery.test(data.toString())) {
                    pon.kill();
                    tail.kill();
                    reject('Unable PPP discovery.');
                }
            });
        });

    }

    switchOffPPPoE() {

        return new Promise((resolve, reject) => {

            let attempts = 0;

            const poff = () => {
                exec(`poff ${process.env.PPP_CONNECTION_NAME}`, () => {
                    exec(`ifconfig | grep ${process.env.PPP_INTERFACE_NAME}`, (error, stdout) => {
                        if (stdout === '') {
                            resolve();
                        }
                        else if (++attempts < 3){
                            setTimeout(() => {
                                poff();
                            }, 1000);
                        }
                        else {
                            reject();
                        }
                    });
                });
            };

            poff();

        });

    }

}

module.exports = PPPManager;
