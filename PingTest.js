require('dotenv').config();
const { exec } = require('child_process');
const EventEmitter = require('events').EventEmitter;

class PingTest extends EventEmitter {

    constructor(pppManager) {

        super();

        this.pppManager = pppManager;
        this.pppManager.on('fatal-disconnect', async () => {
            this.interrupt();
        });
        this.pppManager.on('pppoe-disconnect', async () => {
            this.pause();
        });
        this.pppManager.on('pppoe-connected', async () => {
            this.unpause();
        });

        this.isActive = false;
        this.childProcess = null;

        this.address = null;
        this.packetSize = null;
        this.packetsNumber = null;

        this.transmittedPacketsPattern = /.*^(\d+) packets transmitted.*/m;
        this.receivedPacketsPattern = /.* (\d+) received.*/m;
        this.avgPattern = /.*rtt min\/avg\/max\/mdev = [\d.]+\/([\d.]+)\/.*ms.*/m;

    }

    /**
     * @param address
     * @param packetSize
     * @param packetsNumber
     * @returns {Promise<{transmitted: number, received: number, avgTime: number}>}
     */
    make(address, packetSize, packetsNumber) {

        this.address = address;
        this.packetSize = packetSize;
        this.packetsNumber = packetsNumber;

        return new Promise((resolve, reject) => {

            this.isActive = true;

            const testReadyHandler = (data) => {
                this.removeListener('test-ready', testReadyHandler);
                this.removeListener('interrupt', interruptHandler);
                this.isActive = false;
                resolve(data);
            };

            const interruptHandler = () => {
                this.removeListener('interrupt', interruptHandler);
                this.removeListener('test-ready', testReadyHandler);
                reject('Ping interrupted.');
            }

            this.on('test-ready', testReadyHandler);
            this.on('interrupt', interruptHandler);

            this.test();

        });

    }

    interrupt() {

        if (this.childProcess) {
            this.childProcess.kill();
        }

        this.isActive = false;
        this.emit('interrupt');

    }

    pause() {

        if (this.isActive && this.childProcess) {
            this.childProcess.kill();
        }

    }

    unpause() {

        if (this.isActive) {
            this.test();
        }

    }

    async test() {

        let result = {
            transmitted: 0,
            received: 0,
            avgTime: null
        };

        this.childProcess = exec(`ping -s${this.packetSize} -c${this.packetsNumber} -W2 ${this.address}`, (error, stdout, stderr) => {

            if (error && error.killed) {
                return;
            }

            let matches = this.transmittedPacketsPattern.exec(stdout);
            if (matches && matches[1] !== null) {
                result.transmitted = parseInt(matches[1]);
            }

            matches = this.receivedPacketsPattern.exec(stdout);
            if (matches && matches[1] !== null) {
                result.received = parseInt(matches[1]);
            }

            matches = this.avgPattern.exec(stdout);
            if (matches && matches[1] !== null) {
                result.avgTime = parseFloat(matches[1]);
            }

            this.emit('test-ready', result);

        });

    }

}

module.exports = PingTest;
