const WebSocket = require('ws');
const PPPManager = require('./PPPManager');
const Timekeeper = require('./Timekeeper');
const PingTest = require('./PingTest');
const SpeedTest = require('./SpeedTest');
const Report = require('./Report');
require('dotenv').config();

class Probe {

    constructor() {

        this.wasConnected = false;
        this.prevConnectionState = false;
        this.ws = null;
        this.pppManager = new PPPManager();
        this.timekeeper = new Timekeeper();
        this.pingTest = new PingTest(this.pppManager);
        this.speedTest = new SpeedTest(this.pppManager);

        this.pppManager.on('fatal-disconnect', async () => {

            this.timekeeper.stop();
            process.stdout.write('Error: PPPoE fatal disconnect.\r\n');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'pppoe-state',
                state: 'fatal'
            }));

        });

        this.pppManager.on('pppoe-disconnect', async () => {

            this.timekeeper.pause();
            process.stdout.write('Error: PPPoE disconnect.\r\n');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'pppoe-state',
                state: 'disconnect'
            }));

        });

        this.pppManager.on('pppoe-connected', async () => {

            this.timekeeper.unpause();
            process.stdout.write('PPPoE connect.\r\n');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'pppoe-state',
                state: 'connect'
            }));

        });

    }

    connect() {

        this.ws = null;

        this.ws = new WebSocket(process.env.NEXUS_URL, {
            perMessageDeflate: false
        });

        this.ws.on('error', () => {
            //Do nothing
        })

        this.ws.on('open', () => {
            this.connectedToNexus = true;
            process.stdout.write('Probe connected.\r\n');
            this.auth();
        });

        this.ws.on('message', (message) => {
            try {
                message = JSON.parse(message);

                switch (message.action) {
                    case 'probe-auth':
                        this.authResponseHandler(message);
                        break;
                    case 'start':
                        this.startHandler();

                }
            }
            catch (error) {
                //Do nothing
            }
        });

        this.ws.on('close', async () => {
            if (this.connectedToNexus) {
                this.connectedToNexus = false;
                await this.pppManager.initFatalDisconnect();
                process.stdout.write('Probe disconnected from nexus. Reconnect...\r\n');
            }
            else {
                process.stdout.write('Unable to connect to nexus. Retry...\r\n');
            }

            setTimeout(() => {
                this.connect();
            }, parseInt(process.env.RECONNECT_TIMEOUT));

        });

    }

    auth() {

        this.ws.send(JSON.stringify({
            action: 'probe-auth',
            token: process.env.TOKEN
        }));

    }

    authResponseHandler(message) {

        if (message.ok) {
            process.stdout.write(`Probe authenticated.\r\n`);
            process.stdout.write(`Waiting for the nexus signal...\r\n`);
        }
        else {
            process.stdout.write(`Unable to authenticate. Server response: "${message.error}"\r\n`);
            process.stdout.write(`Retry in ${process.env.RECONNECT_TIMEOUT} ms...\r\n`);
            setTimeout(() => {
                this.auth();
            }, process.env.RECONNECT_TIMEOUT);
        }

    }

    async startHandler() {

        let testHasFatalError = false;

        process.stdout.write('Nexus signal received. Start test...\r\n');
        await Probe.wsSend(this.ws, JSON.stringify({
            action: 'start'
        }));

        this.timekeeper.start();
        await this.pppManager.start();
        let report = new Report();

        try {
            process.stdout.write('Ping primary DNS ');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'primary-dns',
                state: 'start'
            }));
            let pingPrimaryDNSResults = await this.pingTest.make(
                process.env.PRIMARY_DNS_IP,
                process.env.PING_PACKET_SIZE_1,
                process.env.PING_PACKETS_NUMBER
            );
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'primary-dns',
                state: 'finish',
                data: pingPrimaryDNSResults
            }));
            process.stdout.write(`: ${JSON.stringify(pingPrimaryDNSResults)}\r\n`);
            report.pushPingResults(pingPrimaryDNSResults.transmitted, pingPrimaryDNSResults.received, pingPrimaryDNSResults.avgTime);


            process.stdout.write('Ping secondary DNS ');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'secondary-dns',
                state: 'start'
            }));
            let secondaryDNSResults = await this.pingTest.make(
                process.env.SECONDARY_DNS_IP,
                process.env.PING_PACKET_SIZE_1,
                process.env.PING_PACKETS_NUMBER
            );
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'secondary-dns',
                state: 'finish',
                data: secondaryDNSResults
            }));
            process.stdout.write(`: ${JSON.stringify(secondaryDNSResults)}\r\n`);
            report.pushPingResults(secondaryDNSResults.transmitted, secondaryDNSResults.received, secondaryDNSResults.avgTime);


            process.stdout.write('Ping ASBR ');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'asbr',
                state: 'start'
            }));
            let ASBRResults = await this.pingTest.make(
                process.env.ASBR_IP,
                process.env.PING_PACKET_SIZE_2,
                process.env.PING_PACKETS_NUMBER
            );
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'asbr',
                state: 'finish',
                data: ASBRResults
            }));
            process.stdout.write(`: ${JSON.stringify(ASBRResults)}\r\n`);
            report.pushPingResults(ASBRResults.transmitted, ASBRResults.received, ASBRResults.avgTime);


            process.stdout.write('Speedtest ');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'speedtest',
                state: 'start'
            }));
            let speedtestResults = await this.speedTest.make();
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'speedtest',
                state: 'finish',
                data: speedtestResults
            }));
            process.stdout.write(`: ${JSON.stringify(speedtestResults)}\r\n`);
            report.pushSpeedtestResults(speedtestResults);


            process.stdout.write('Waiting');
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'wait',
                state: 'start'
            }));
            await this.wait(parseInt(process.env.DELAY_BETWEEN_TESTS));
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'wait',
                state: 'finish'
            }));
            process.stdout.write('.\r\n');
        }
        catch (error) {
            process.stdout.write('Test circle ended with error!\r\n');
            testHasFatalError = true;
        }
        finally {
            process.stdout.write('Finish test.\r\n');

            let timekeeperData = this.timekeeper.stop();
            await this.pppManager.stop();

            if (testHasFatalError) {
                report.makeUnsuccessful();
            }
            else {
                report.pushTimekeeperData(timekeeperData);
            }

            let reportData = report.generate();

            process.stdout.write(`Report:\r\n ${JSON.stringify(reportData)}\r\n\r\n\r\n`);
            await Probe.wsSend(this.ws, JSON.stringify({
                action: 'test',
                subAction: 'report',
                data: reportData
            }));

        }

    }

     wait(t) {

        return new Promise((resolve, reject) => {

            const fatalDisconnectHandler = () => {

                this.pppManager.removeListener('fatal-disconnect', fatalDisconnectHandler);
                reject('Waiting interrupted.');

            };

            this.pppManager.on('fatal-disconnect', fatalDisconnectHandler);

            setTimeout(() => {
                resolve();
            }, t);

        });

    }

    /**
     * @param ws
     * @param message
     * @returns {Promise<unknown>}
     */
    static wsSend(ws, message) {

        return new Promise(resolve => {
            try {
                ws.send(message, () => {
                    resolve();
                });
            }
            catch (error) {
                resolve();
            }
        });

    }

}

module.exports = Probe;
