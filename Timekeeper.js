class Timekeeper {

    constructor() {

        this.interrupts = 0;
        this.downtime = 0;
        this.startedAt = null;
        this.pausedAt = null;

    }

    /**
     * Start timekeeper. Initial state - paused.
     */
    start() {

        this.interrupts = 0;
        this.startedAt = (new Date()).getTime();
        this.pausedAt = (new Date()).getTime();
        this.downtime = 0;

    }

    /**
     * @returns {{downtime: number, totalTime: number, interrupts: number}|null}
     */
    stop() {

        if (this.startedAt) {
            if (this.pausedAt) {
                this.downtime += (new Date()).getTime() - this.pausedAt;
            }

            let interrupts = this.interrupts;
            let downtime = this.downtime;
            let totalTime = (new Date()).getTime() - this.startedAt;

            this.interrupts = 0;
            this.startedAt = null;
            this.pausedAt = null;
            this.downtime = 0;

            return {
                totalTime,
                downtime,
                interrupts
            }
        }

        return null;

    }

    pause() {

        if (!this.pausedAt) {
            this.interrupts++;
            this.pausedAt = (new Date()).getTime();
        }

    }

    unpause() {

        if (this.pausedAt) {
            this.downtime += (new Date()).getTime() - this.pausedAt;
            this.pausedAt = null;
        }

    }

}

module.exports = Timekeeper;
