require('dotenv').config();

class Report {

    constructor() {

        this.isSuccessful = true;
        this.isInterrupted = false;
        this.readyness = 0;
        this.pingDelayIsOk = false;
        this.pingLossRateIsOk = false;
        this.speedIsOk = false;

        this.pingTransmitted = 0;
        this.pingReceived = 0;
        this.avg = 0;
        this.speedtestResults = {
            rx: {
                speed: 0,
                rate: 0
            },
            tx: {
                speed: 0,
                rate: 0
            }
        };

    }

    pushPingResults(transmitted, received, avg) {

        this.pingTransmitted += transmitted;
        this.pingReceived += received;
        this.avg = avg ? (this.avg + avg)/2: this.avg;

    }

    makeUnsuccessful() {
        this.isSuccessful = false;
    }

    /**
     * @param {{rx: {speed: number, rate: number}, tx: {speed: number, rate: number}}} results
     */
    pushSpeedtestResults(results) {
        this.speedtestResults = results;
    }

    /**
     * @param {{downtime: number, totalTime: number, interrupts: number}|null} data
     */
    pushTimekeeperData(data) {

        if (data) {
            if (data.interrupts > 0) {
                this.isInterrupted = true;
            }

            this.readyness = 100 - (data.downtime / data.totalTime * 100);
        }
        else {
            this.makeUnsuccessful();
        }

    }

    generate() {

        return {
            isSuccessful: this.isSuccessful,
            isInterrupted: this.isInterrupted,
            readyness: this.readyness,
            pingDelayIsOk: this.avg < process.env.PING_DELAY_TRASHOLD,
            pingLossRateIsOk: (this.pingReceived/this.pingTransmitted*100) > (100 - process.env.PING_LOSS_TRASHOLD_PERCENT),
            speedIsOk: this.speedtestResults.tx.rate > process.env.SPEED_TRASHOLD_PERCENT && this.speedtestResults.rx.rate > process.env.SPEED_TRASHOLD_PERCENT
        }

    }

}

module.exports = Report;
