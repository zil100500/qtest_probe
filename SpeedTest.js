require('dotenv').config();
const { exec } = require('child_process');
const EventEmitter = require('events').EventEmitter;

class SpeedTest extends EventEmitter{

    constructor(pppManager) {

        super();

        this.pppManager = pppManager;
        this.pppManager.on('fatal-disconnect', async () => {
            this.interrupt();
        });
        this.pppManager.on('pppoe-disconnect', async () => {
            this.pause();
        });
        this.pppManager.on('pppoe-connected', async () => {
            this.unpause();
        });

        this.isActive = false;
        this.childProcess = null;

    }

    make() {

        return new Promise((resolve, reject) => {

            this.isActive = true;

            const testReadyHandler = (data) => {
                this.removeListener('test-ready', testReadyHandler);
                this.removeListener('interrupt', interruptHandler);
                this.isActive = false;
                resolve(data);
            };

            const interruptHandler = () => {
                this.removeListener('interrupt', interruptHandler);
                this.removeListener('test-ready', testReadyHandler);
                reject('Speedtest interrupted.');
            }

            this.on('test-ready', testReadyHandler);
            this.on('interrupt', interruptHandler);

            this.test();

        });

    }

    interrupt() {

        if (this.childProcess) {
            this.childProcess.kill();
        }

        this.isActive = false;
        this.emit('interrupt');

    }

    pause() {

        if (this.isActive && this.childProcess) {
            this.childProcess.kill();
        }

    }

    unpause() {

        if (this.isActive) {
            this.test();
        }

    }

    async test() {

        let result = {
            rx: {
                speed: 0,
                rate: 0
            },
            tx: {
                speed: 0,
                rate: 0
            }
        };

        try {
            result.tx = await this.exec('up');
            result.rx = await this.exec('down');
            this.emit('test-ready', result);
        }
        catch (error) {
            //Do nothing
        }

    }

    exec(direction='up') {

        return new Promise((resolve, reject) => {

            let result = {
                speed: 0,
                rate: 0
            };

            this.childProcess = exec(
                `expect -f speedtest_${direction}.exp ${process.env.SPEEDTEST_LOCAL_FILE} ${process.env.SPEEDTEST_USERNAME} ${process.env.SPEEDTEST_REMOTE_FILE} ${process.env.SPEEDTEST_PASSWORD} ${process.env.SPEEDTEST_SERVER_IP}`,
                (error, stdout, stderr) => {
                    if (error && error.killed) {
                        reject('Interrupted');
                    }

                    if (stdout.match(/.*Exit status 0.?/m) !== null) {
                        let matches = stdout.match(/.*Bytes per second: sent ([\d.]+), received ([\d.]+).?/m);
                        if (matches !== null) {
                            if (direction === 'up') {
                                result.speed = parseFloat(matches[1]);
                                result.rate = result.speed / parseFloat(process.env.DECLARED_TX_SPEED) * 100;
                                result.rate = result.rate > 100 ? 100 : result.rate;
                            }
                            else if (direction === 'down') {
                                result.speed = parseFloat(matches[2]);
                                result.rate = result.speed / parseFloat(process.env.DECLARED_RX_SPEED) * 100;
                                result.rate = result.rate > 100 ? 100 : result.rate;
                            }
                        }
                    }

                    resolve(result);
                });

        });

    }

}

module.exports = SpeedTest;
